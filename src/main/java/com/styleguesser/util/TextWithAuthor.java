package com.styleguesser.util;

public class TextWithAuthor {
    private String text;
    private String author;

    public TextWithAuthor(String text, String author) {
        this.text = text;
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public String getAuthor() {
        return author;
    }
}
