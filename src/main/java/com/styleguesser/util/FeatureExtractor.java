package com.styleguesser.util;

import java.util.List;

public interface FeatureExtractor {
    ExtractedFeatures extractFeatures(TextWithAuthor textWithAuthor);

    TrainData getTrainData(List<TextWithAuthor> texts, List<String> classes);
}
