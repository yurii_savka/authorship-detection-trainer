package com.styleguesser.util;

import java.util.HashMap;
import java.util.Set;

public class ExtractedFeatures {
    public static final String AUTHOR_UNKNOWN = "X";

    private String author = "";
    private HashMap<String, Double> features;

    public ExtractedFeatures() {
        features = new HashMap<String, Double>();
    }

    public double get(String fieldName) {
        return features.get(fieldName);
    }

    public void set(String fieldName, double value) {
        features.put(fieldName, value);
    }

    public int size() {
        return features.size();
    }

    public Set<String> fieldNames() {
        return features.keySet();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String toString() {
        String s = "";
        s += "Author: " + getAuthor() + ", ";
        for (String field : fieldNames()) {
            s += field + ": " + get(field) + ", ";
        }

        return s;
    }
}
