package com.styleguesser.util;

import org.languagetool.language.Ukrainian;
import org.languagetool.tokenizers.SRXSentenceTokenizer;
import org.languagetool.tokenizers.uk.UkrainianWordTokenizer;

import java.util.*;
import java.util.stream.Collectors;

public class SimpleFeatureExtractor implements FeatureExtractor {
    public static String[] FIELDS = new String[]{
            "Average Sentence Length",
            "Average Sentence Complexity",
            "Average Word Length",
            "Type-Token Ratio",
            "Hapax Legomana Ratio",
    };
    SRXSentenceTokenizer sentTokenizer = new SRXSentenceTokenizer(new Ukrainian());
    UkrainianWordTokenizer wordTokenizer = new UkrainianWordTokenizer();

    private List<String> filterRealWords(List<String> words) {
        return words
                .stream()
                .filter(word -> word.length() != 1 || !" :—.,!?;()\n\r".contains(word))
                .collect(Collectors.toList());
    }

    private long countSeparators(List<String> words) {
        return words
                .stream()
                .filter(word -> word.length() == 1 && ":,;".contains(word))
                .count();
    }

    public ExtractedFeatures extractFeatures(TextWithAuthor textWithAuthor) {
        String text = textWithAuthor.getText();
        String author = textWithAuthor.getAuthor();
        ExtractedFeatures features = new ExtractedFeatures();
        List<String> sentences = sentTokenizer.tokenize(text);
        List<String> allWords = new ArrayList<>();

        features.setAuthor(author);

        long sentenceTotalLength = 0;
        long sentenceTotalComplexity = 0;
        for (String sentence : sentences) {
            List<String> words = wordTokenizer.tokenize(sentence);
            List<String> realWords = filterRealWords(words);
            sentenceTotalComplexity += countSeparators(words) + 1;
            sentenceTotalLength += realWords.size();
            allWords.addAll(realWords);
        }
        features.set("Average Sentence Length", (double) sentenceTotalLength / sentences.size());
        features.set("Average Sentence Complexity", (double) sentenceTotalComplexity / sentences.size());

        long wordTotalLength = 0;
        for (String word : allWords) {
            wordTotalLength += word.length();
        }
        features.set("Average Word Length", (double) wordTotalLength / allWords.size());

        Set<String> wordsOnce = new HashSet<>();
        Set<String> wordsTwice = new HashSet<>();
        for (String word : allWords) {
            String lWord = word.toLowerCase();
            if (!wordsOnce.contains(lWord)) {
                wordsOnce.add(lWord);
            } else if (!wordsTwice.contains(lWord)) {
                wordsTwice.add(lWord);
            }
        }

        features.set("Type-Token Ratio", (double) wordsOnce.size() / allWords.size());
        features.set("Hapax Legomana Ratio", (double) (wordsOnce.size() - wordsTwice.size()) / allWords.size());


        System.out.println(features);
        return features;
    }

    public TrainData getTrainData(List<TextWithAuthor> textsWithAuthors, List<String> classes) {
        TrainData tData = new TrainData();
        tData.setClasses(classes);
        tData.setFields(new ArrayList<>(Arrays.asList(FIELDS)));

        List<ExtractedFeatures> featuresList = textsWithAuthors
                .stream()
                .map(this::extractFeatures)
                .collect(Collectors.toList());
        tData.setFeaturesList(featuresList);

        return tData;
    }
}
